"""users table

Revision ID: f2a8b0d4a73b
Revises: b349c1f0032b
Create Date: 2018-11-23 08:32:18.325000

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f2a8b0d4a73b'
down_revision = 'b349c1f0032b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('session_id', sa.String(length=20), nullable=True))
    op.create_index(op.f('ix_user_session_id'), 'user', ['session_id'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_user_session_id'), table_name='user')
    op.drop_column('user', 'session_id')
    # ### end Alembic commands ###
