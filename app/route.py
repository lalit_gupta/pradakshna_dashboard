from flask import jsonify
from app import app
from app import login,db
from flask_login import current_user,login_user,logout_user,login_required
from models import User
from forms import LoginForm,RegisterForm
from flask import render_template,redirect,url_for
from werkzeug.urls import url_parse
from flask import request
from config import Config
import requests
import datetime

@app.route('/health',methods=['GET'])
def health():
    return jsonify(get_geo_location(lat=24.4330688,long=77.1611144))

def get_geo_location(lat,long):
    api_key=Config.GOOGLE_API_KEY
    url='https://maps.googleapis.com/maps/api/geocode/json?key={}&latlng={},{}'.format(api_key,lat,long)
    response=requests.get(url)
    return response.json()['results'][0]['formatted_address']


@app.route('/index')
#@login_required
def index():
    return render_template('index.html')

@app.route('/login',methods=['GET','POST'])
def login():
    print request.form
    form = LoginForm()
    print "form_data={}".format(form.data)
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    if form.validate_on_submit():
        user=User.query.filter_by(username=form.username.data).first()
        user.session_id=datetime.datetime.now().strftime("%Y%m%d%H%S%M")
        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('login'))
        login_user(user,remember=form.remember_me.data)
        next_page=request.args.get('next')
        if not next_page or url_parse(next_page).netloc !='':
            next_page=url_for('index')
        return redirect(next_page)
        return redirect(url_for('index'))
    return render_template('login.html',form=form)

@app.route('/register',methods=['GET','POST'])
def register():
    if current_user.is_authenticated:
        return redirect('login')
    register_form=RegisterForm()
    if register_form.validate_on_submit():
        user=User(username=register_form.username.data)
        user.set_password(register_form.password.data)
        db.session.add(user)
        db.session.commit()
        return redirect('login')
    return render_template('register.html',form=register_form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/get_user_stat',methods=['GET'])
@login_required
def get_user_stat():
    data={
        "pie_chart":[
            {"activation_pending":"2"},
            {"signed_in_today":"2"},
            {"not_signed_in_today":"2"}
        ]
            ,
        "table_data":
            {"activation_pending":[{"employee_name":"user1","added on":"18-11-2018"},
                                   {"employee_name": "user2", "added on": "2-11-2018"},
                                   {"employee_name": "user3", "added on": "20-11-2018"}]
                ,
             "signed_in_today":[
                 {"employee_id":"1234","employee_name":"user23",
                  "last_signed_in_time":"20-11-2018:23:53",
                  "last_sync_time":"21-11-2018:20:50","location":"Powai,Mumbai"},

                 {"employee_id": "1214", "employee_name": "user24",
                  "last_signed_in_time": "22-11-2018:10:53",
                  "last_sync_time": "22-11-2018:20:50", "location": "Kurla,Mumbai"}
             ],
             "not_signed_in_today":[
                 {"employee_id": "122", "employee_name": "user123",
                  "mobile_no":"1234554321",
                  "last_signed_in_time": "22-11-2018:10:53",
                  "last_sync_time": "22-11-2018:20:50", "last_sign_out_time": "22-11-2018:21:05"},
                 {"employee_id": "1224", "employee_name": "user22",
                  "mobile_no": "1232354321",
                  "last_signed_in_time": "19-11-2018:10:53",
                  "last_sync_time": "20-11-2018:20:50", "last_sign_out_time": "21-11-2018:21:05"}
             ]
             }
    }


    return jsonify(data)